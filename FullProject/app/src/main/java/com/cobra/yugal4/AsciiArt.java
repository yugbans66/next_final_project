package com.cobra.yugal4;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.Q)
public class AsciiArt {
    List<Double> avgs = new ArrayList<>();

    public String convertToAscii(Bitmap img) {
        for (int i = 0; i < img.getHeight(); i++) {
            for (int j = 0; j < img.getWidth(); j++) {
                int pixcol = img.getPixel(j, i);
                double value = ((double) Color.red(pixcol) / 3) + ((double) Color.green(pixcol) / 3) + ((double) Color.blue(pixcol) / 3);
                avgs.add(value);
            }
            avgs.add(-1d);
        }

        double max = 0;
        double min = 255;
        for (double a : avgs) {
            if (a != -1) {
                if (a > max) max = a;
                if (a < min) min = a;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < avgs.size(); i++) {
            if (avgs.get(i) != -1) {
                avgs.set(i, map(avgs.get(i), min, max));

                sb.append(strChar(avgs.get(i)));
            } else sb.append("\n");
        }

        return sb.toString();
    }

    double map(double x, double in_min, double in_max) {
        return (x - in_min) * (255d) / (in_max - in_min) + 0d;
    }

    public String strChar(double g) {
        String str;
        if (g >= 240) {
            str = " ";
        } else if (g >= 210) {
            str = ".";
        } else if (g >= 190) {
            str = "*";
        } else if (g >= 170) {
            str = "+";
        } else if (g >= 120) {
            str = "^";
        } else if (g >= 110) {
            str = "&";
        } else if (g >= 80) {
            str = "8";
        } else if (g >= 60) {
            str = "#";
        } else {
            str = "@";
        }
        return str;
    }
}
