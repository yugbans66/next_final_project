package com.cobra.yugal4;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_DENIED;

public class MainActivity extends AppCompatActivity {

    Uri sourceUri;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        Button cameraButton = findViewById(R.id.cameraButton);
        Button galleryButton = findViewById(R.id.galleryButton);

        cameraButton.setOnClickListener(view -> {
            sourceUri = Uri.fromFile(new File(getExternalCacheDir().getAbsolutePath() + "/rawPic.jpg"));
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePicture.putExtra(MediaStore.EXTRA_OUTPUT, sourceUri);
            startActivityForResult(takePicture, 0);
        });

        galleryButton.setOnClickListener(view -> {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, 1);
        });


        if (ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE) == PERMISSION_DENIED)
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, 2);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void doAsciiArt() {
        new Thread(() -> {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), sourceUri);
                runOnUiThread(() -> changeStatus("Converting image to ASCII."));

                Bitmap bmp = getResizedBitmap(bitmap, 200);

                AsciiArt a = new AsciiArt();
                String txt = a.convertToAscii(bmp);

                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("hoe", txt);
                clipboard.setPrimaryClip(clip);

                bmp = textAsBitmap(txt);

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 0, bos);
                FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + "/hoe2.jpg");
                fos.write(bos.toByteArray());
                fos.flush();
                fos.close();

                runOnUiThread(() -> changeStatus("Done! Click image to share."));

                Bitmap finalBmp = bmp;
                runOnUiThread(() -> {
                    ImageView imageView = findViewById(R.id.imageView);
                    imageView.setImageBitmap(finalBmp);
                    imageView.setOnClickListener(view -> {
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("image/jpeg");
                        startActivity(Intent.createChooser(share, "Share Image"));
                    });

                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public Bitmap textAsBitmap(String text) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        int textSize = 20;
        paint.setTextSize(textSize);
        paint.setColor(Color.BLACK);
        paint.setTextScaleX(1.7f);
        paint.setTypeface(Typeface.create(Typeface.MONOSPACE, Typeface.BOLD));
        paint.setTextAlign(Paint.Align.CENTER);
        float baseline = -paint.ascent();
        String[] strs = text.split("\n");
        int width = (int) (strs[1].length() * textSize * 1.5f + 0.5f);
        int height = (int) (baseline + paint.descent() + 0.5f + strs.length * textSize);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawColor(Color.WHITE);
        int y = 0;
        for (String str : strs) {
            canvas.drawText(str, width / 2, baseline + y, paint);
            y += textSize;
        }
        return image;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        ImageView imageView = findViewById(R.id.imageView);

        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    imageView.setImageURI(sourceUri);
                    doAsciiArt();
                    break;
                }
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    imageView.setImageURI(selectedImage);
                    sourceUri = selectedImage;
                    doAsciiArt();
                    break;
                }
            case 2:
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Permissions required fpr app to work!", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, 2);
                }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        image = correctRotation(image);
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private Bitmap correctRotation(Bitmap imageBitmap) {
        try {
            ExifInterface ei = new ExifInterface(sourceUri.getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    imageBitmap = RotateBitmap(imageBitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    imageBitmap = RotateBitmap(imageBitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    imageBitmap = RotateBitmap(imageBitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageBitmap;
    }

    private Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void changeStatus(String status) {
        ((TextView) findViewById(R.id.statusText)).setText(status);
    }
}